<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Question extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('QUESTION', function(Blueprint $table) {
		    $table->increments('ID');
		    $table->text('NUMBER')->nullable();
		    $table->integer('HOURS')->nullable();
		    $table->integer('IDREP')->unsigned();
		    $table->text('THEME')->nullable();
		
		    $table->index('idrep','fk_question_report1_idx');
		
		    $table->foreign('idrep')
		        ->references('id')->on('report')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('QUESTION');
    }
}
