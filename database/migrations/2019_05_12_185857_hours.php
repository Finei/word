<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Hours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('HOURS', function(Blueprint $table) {
		    $table->increments('ID');
		    $table->integer('LHOUR')->nullable();
		    $table->integer('PRHOUR')->nullable();
		    $table->integer('LABHOUR')->nullable();
		    $table->integer('INDHOUR')->nullable();
		    $table->integer('SRHOUR')->nullable();
		    $table->integer('THOUR')->nullable();
		    $table->integer('IDMODULE')->nullable()->unsigned();
		    $table->integer('IDTHEME')->nullable()->unsigned();
		    $table->integer('IDSEMESTR')->nullable()->unsigned();
		
		    $table->index('idtheme','fk_hours_theme1_idx');
		    $table->index('idmodule','fk_hours_module1_idx');
		    $table->index('idsemestr','fk_hours_semestr1_idx');
		
		    $table->foreign('idtheme')
		        ->references('id')->on('theme')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		    $table->foreign('idmodule')
            ->references('id')->on('module')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
            
		    $table->foreign('idsemestr')
          ->references('id')->on('semestr')
          ->onDelete('cascade')
          ->onUpdate('cascade');
		
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('HOURS');
    }
}
