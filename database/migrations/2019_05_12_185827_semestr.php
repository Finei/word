<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Semestr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SEMESTR', function(Blueprint $table) {
		    $table->increments('ID');
		    $table->integer('NUMBER')->nullable();
		    $table->string('TYPEWORK', 45)->nullable();
		    $table->string('TYPEEXAM', 45)->nullable();
		    $table->integer('IDPREPYEAR')->unsigned();
		
		    $table->index('idprepyear','fk_semestr_prepyear1_idx');
				
		    $table->foreign('idprepyear')
		        ->references('id')->on('prepyear')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');	
		
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('SEMESTR');
    }
}
