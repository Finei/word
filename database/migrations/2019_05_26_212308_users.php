<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table) {		
		    $table->increments('id');
		    $table->string('login');
		    $table->string('password');
		    $table->string('name', 45)->nullable();
		    $table->string('surname', 45)->nullable();
		    $table->string('otch', 45)->nullable();
		    $table->string('role', 100)->nullable();
        $table->string('type', 1);
		    $table->integer('IDDEPART')->nullable()->unsigned();

        $table->index('iddepart','fk_users_departments1_idx');

		    $table->foreign('iddepart')
		        ->references('id')->on('departments')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
        
        $table->rememberToken();	
		    $table->timestamps();		
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('users');
    }
}
