<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Agreecomment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreecomment', function(Blueprint $table) {
		    $table->increments('id');
		    $table->text('comment')->nullable();
		    $table->integer('idagree')->unsigned();
		    $table->integer('typeagree')->nullable();
		
		    $table->index('idagree','fk_agreecomment_agreement1_idx');
		
		    $table->foreign('idagree')
		        ->references('id')->on('agreement')
                ->onDelete('cascade')
                ->onUpdate('cascade');
		
		    $table->timestamps();
		
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('agreecomment');
    }
}
