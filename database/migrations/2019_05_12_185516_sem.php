<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SEM', function(Blueprint $table) {
		
		    $table->increments('ID');
		    $table->integer('NUMBER')->nullable();
		    $table->text('NAME')->nullable();
		    $table->integer('HOURS')->nullable();
		    $table->integer('IDREP')->unsigned();
		
		    $table->index('idrep','fk_sem_report1_idx');
		
		    $table->foreign('idrep')
		        ->references('id')->on('report')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');;
		
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('SEM');
    }
}
