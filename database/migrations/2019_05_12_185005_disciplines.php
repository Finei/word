<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Disciplines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disciplines', function(Blueprint $table) {            
		    $table->increments('id');
		    $table->string('title', 100);
		    $table->integer('IDSPEC')->unsigned();
		
		    $table->index('idspec','fk_disciplines_specialities1_idx');
		
		    $table->foreign('idspec')
		        ->references('id')->on('specialities')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');;
		
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::drop('disciplines');
    }
}
