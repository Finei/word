<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Lab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LAB', function(Blueprint $table) {
		    $table->increments('ID');
		    $table->text('NAME')->nullable();
		    $table->integer('HOURS')->nullable();
		    $table->integer('NUMBER')->nullable();
		    $table->integer('IDTHEME')->unsigned();
		    $table->integer('IDREP')->unsigned();
		
        $table->index('idtheme','fk_lab_theme1_idx');
		    $table->index('idrep','fk_lab_report1_idx');
		
		    $table->foreign('idtheme')
		        ->references('id')->on('theme')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		    $table->foreign('idrep')
		        ->references('id')->on('report')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('LAB');
    }
}
