<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Bal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('BAL', function(Blueprint $table) {
		    $table->increments('ID');
		    $table->integer('TEXTBAL')->nullable();
		    $table->integer('GRAPHBAL')->nullable();
		    $table->integer('SECUREBAL')->nullable();
		    $table->integer('IDREP')->unsigned();
		
		    $table->index('idrep','fk_bal_report1_idx');
		
		    $table->foreign('idrep')
		        ->references('id')->on('report')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('BAL');
    }
}
