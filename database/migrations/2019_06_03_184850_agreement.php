<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Agreement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement', function(Blueprint $table) {
		    $table->increments('id');
		    $table->string('ZKstatus', 1)->nullable()->default(null);
		    $table->string('GMRstatus', 1)->nullable()->default(null);
		    $table->string('GNMVstatus', 1)->nullable()->default(null);
		    $table->integer('idrep')->unsigned();
		
		    $table->index('idrep','fk_agreement_report1_idx');
		
		    $table->foreign('idrep')
		        ->references('id')->on('report')
                ->onDelete('cascade')
                ->onUpdate('cascade');
		
		    $table->timestamps();
		
		});        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('agreement');
    }
}
