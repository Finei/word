<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Specialities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('specialities', function(Blueprint $table) {		
         
          $table->increments('id');
          $table->string('title', 100);
          $table->integer('IDDEPART')->unsigned();
          $table->string('SPCODE', 45)->nullable();
          $table->integer('AREA_ID')->unsigned();
      
          $table->index('iddepart','fk_specialities_departments_idx');
          $table->index('area_id','fk_specialities_area1_idx');
      
          $table->foreign('iddepart')
              ->references('id')->on('departments')
              ->onDelete('cascade')
              ->onUpdate('cascade');
      
          $table->foreign('area_id')
              ->references('id')->on('area')
              ->onDelete('cascade')
              ->onUpdate('cascade');
      });
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::drop('specialities');
    }
}
