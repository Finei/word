<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Report extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('REPORT', function(Blueprint $table) {
		    $table->increments('ID');
		    $table->string('DEVELOPER', 45)->nullable();
		    $table->text('PURPOSE')->nullable();
		    $table->text('TASK')->nullable();
		    $table->text('INDIVIDUALWORK')->nullable();
		    $table->integer('CREDITCOUNT')->nullable();
		    $table->integer('MODULESCOUNT')->nullable();
		    $table->integer('ZMMODULESCOUNT')->nullable();
		    $table->string('ISNESSESORY', 1)->nullable();
		    $table->integer('YEAR')->nullable();
		    $table->integer('USERID')->nullable();
		    $table->integer('THOURS')->nullable();
		    $table->integer('IDDESC')->unsigned();
		    $table->integer('IDSPEC')->unsigned();
		    $table->integer('IDPREP')->unsigned();
		
		    $table->index('iddesc','fk_report_disciplines1_idx');
		    $table->index('idspec','fk_report_specialities1_idx');
		    $table->index('idprep','fk_report_preparations1_idx');
		
		    $table->foreign('iddesc')
		        ->references('id')->on('disciplines')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		    $table->foreign('idspec')
		        ->references('id')->on('specialities')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		    $table->foreign('idprep')
		        ->references('id')->on('preparations')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('REPORT');
    }
}
