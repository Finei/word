<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Beable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('BEABLE', function(Blueprint $table) {
		    $table->increments('ID');
		    $table->text('NAME')->nullable();
		    $table->integer('IDREP')->unsigned();
		
		    $table->index('idrep','fk_beable_report1_idx');
		
		    $table->foreign('IDREP')
		        ->references('ID')->on('report')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('BEABLE');
    }
}
