<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Praktika extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PRAKTIKA', function(Blueprint $table) {
		    $table->increments('ID');
		    $table->integer('NUMBER')->nullable();
		    $table->text('NAME')->nullable();
		    $table->integer('HORS')->nullable();
		    $table->integer('IDREP')->unsigned();
		
		    $table->index('idrep','fk_praktik_report1_idx');
		
		    $table->foreign('idrep')
		        ->references('id')->on('report')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('PRAKTIKA');
    }
}
