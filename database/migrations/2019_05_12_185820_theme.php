<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Theme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('THEME', function(Blueprint $table) {
		
		    $table->increments('ID');
		    $table->text('TITLE')->nullable();
		    $table->text('DESCR')->nullable();
		    $table->integer('BAL')->nullable();
		    $table->integer('NUMBER')->nullable();
		    $table->integer('IDMODULE')->unsigned();
		
		    $table->index('idmodule','fk_theme_module1_idx');
		
		    $table->foreign('idmodule')
		        ->references('id')->on('module')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('THEME');
    }
}
