<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Lit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LIT', function(Blueprint $table) {
		    $table->increments('ID');
		    $table->text('NAME')->nullable();
		    $table->integer('TYPE')->nullable();
		    $table->integer('IDREP')->unsigned();
		
		    $table->index('idrep','fk_lit_report1_idx');
		
		    $table->foreign('idrep')
		        ->references('id')->on('report')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('LIT');
    }
}
