<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Module extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MODULE', function(Blueprint $table) {
		    $table->increments('ID');
		    $table->integer('NUMBER')->nullable();
		    $table->string('NAME', 100)->nullable();
		    $table->integer('TASKBAL')->nullable();
		    $table->integer('EXAMBAL')->nullable();
		    $table->integer('IDREP')->unsigned();
		
		    $table->index('idrep','fk_module_report1_idx');
		
		    $table->foreign('idrep')
		        ->references('id')->on('report')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
		
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('MODULE');
    }
}
