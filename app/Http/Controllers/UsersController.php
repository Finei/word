<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use DB;

class UsersController extends Controller
{
    public function index() {        
        $users = DB::select("select users.*,usertype.title as typetitle from users left join usertype on usertype.type = users.type");
        $departments = DB::select("select * from departments");

        return view('users', compact('users','departments'));
    }
    public function create(Request $request) {
        $id = DB::table('users')->insertGetId([
            'login' => $request->login,
            'password' => bcrypt($request->password),
            'name' => $request->name,
            'surname' => $request->surname,
            'otch' => $request->otch,
            'role' => $request->role,
            'type' => $request->type,
            'iddepart'=> $request->iddepart==0?null:$request->iddepart,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        return $id;
    }
    public function update(Request $request) {
        DB::table('users')
            ->where('id', $request->id)        
            ->update([
            'login' => $request->login,
            'password' => bcrypt($request->password),
            'name' => $request->name,
            'surname' => $request->surname,
            'otch' => $request->otch,
            'role' => $request->role,
            'type' => $request->type,
            'iddepart'=> $request->iddepart,
        ]);
        return "success";
    }
    public function delete(Request $request) {
        DB::table('users')->where('id', '=', $request->id)->delete();
        return $request;
    }
}
