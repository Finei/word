<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use DB;

class AreasController extends Controller
{
    public function index() {        
        $areas = DB::select("select id, name ,code from area");

        return view('areas', compact('areas'));
    }
    public function create(Request $request) {
        $id = DB::table('area')->insertGetId([
            'name' => $request->name,
            'code' => $request->code,
        ]);
        return $id;
    }
    public function update(Request $request) {
        DB::table('area')
            ->where('id', $request->id)        
            ->update([
            'name' => $request->name,
            'code' => $request->code
        ]);
        return "success";
    }
    public function delete(Request $request) {
        DB::table('area')->where('id', '=', $request->id)->delete();
        return $request;
    }
}
