<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use DB;

class DepartmentsController extends Controller
{
    public function index() {        
        $departments = DB::select("select departments.id, departments.title, users.name,users.surname from departments left join users on users.iddepart = departments.id");

        return view('departments', compact('departments'));
    }
    public function create(Request $request) {
        $id = DB::table('departments')->insertGetId([
            'title' => $request->title,
        ]);
        return $id;
    }
    public function update(Request $request) {
        DB::table('departments')
            ->where('id', $request->id)        
            ->update([
            'title' => $request->title
        ]);
        return "success";
    }
    public function delete(Request $request) {
        DB::table('departments')->where('id', '=', $request->id)->delete();
        return $request;
    }
}
