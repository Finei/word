<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class WordController extends Controller
{
    public function createWordDocx($id)
    {
        $reports = DB::select(
            @"select r.*, dis.title as distitle, dis.id as disid,
            sp.title as spectitle, sp.spcode, sp.id as specid,
            dep.title as deptitle, dep.id as depid,
            pr.title as prtitle, pr.id as prid,
            users.name as userName, users.surname as userSurname, users.Role as userRole, users.otch as userOtch,
            area.name as areaName, area.CODE as areaCode
               from report r
            left join users on users.id = r.USERID
            left join disciplines dis on dis.id = r.iddesc
            left join specialities sp on sp.id = r.idspec
            left join departments dep on dep.id = sp.iddepart
            left join preparations pr on pr.id = r.idprep
            left join area on area.id = sp.area_id
            where r.id = ?
        ",[$id]);
        $iknow = DB::select(
            @"select * from know where idrep = ?
        ",[$id]);
        $ipraktika = DB::select(
            @"select * from praktika where idrep = ?
        ",[$id]);
        $ibeable = DB::select(
            @"select * from beable where idrep = ?
        ",[$id]);
        $bal = DB::select(
            @"select * from bal where idrep = ?
        ",[$id]);
        $isem = DB::select(
            @"select * from sem where idrep = ?
        ",[$id]);
        $iquestion = DB::select(
            @"select * from question where idrep = ?
        ",[$id]);
        $ilit = DB::select(
            @"select * from lit where idrep = ?
        ",[$id]);
        $iprepyear = DB::select(
            @"select * from prepyear where idrep = ?
        ",[$id]);
        $isemestr = DB::select(
            @"select semestr.*,hours.* from prepyear 
            left join semestr on semestr.idprepyear = prepyear.id
            left join hours on semestr.id = hours.idsemestr
            where idrep = ?
        ",[$id]);
        $imodule = DB::select(
            @"select * from module where idrep = ?
        ",[$id]);
        $itheme = DB::select(
            @"select theme.TITLE,theme.DESCR,theme.BAL,theme.NUMBER,theme.IDMODULE as MIDMODULE,hours.* from module 
            left join theme  on theme.idmodule = module.id 
            left join hours on hours.idtheme = theme.id
            where idrep =?
        ",[$id]);
        $ilab = DB::select(
            @"select lab.* from module 
            left join theme  on theme.idmodule = module.id 
            left join hours on hours.idtheme = theme.id
            left join lab on lab.idtheme = theme.id
            where module.idrep = ?
        ",[$id]);
        $disciplines = DB::select("select * from disciplines");
        $departments = DB::select("select * from departments");
        $specialities = DB::select("select * from specialities");
        $preparations = DB::select("select * from preparations");
        $ireport = array_shift($reports);
        $ibal = array_shift($bal);



        $wordTest = new \PhpOffice\PhpWord\PhpWord();

        $document =  $wordTest->loadTemplate(storage_path('sample.docx')); 
        $document->setValue('${cafedra}', $ireport->deptitle);
        $document->setValue('${discipline}', mb_strtoupper($ireport->distitle));
        $document->setValue('${disciplinee}', $ireport->distitle);
        $document->setValue('${prep}', $ireport->prtitle);
        $document->setValue('${speciality}', mb_strtoupper($ireport->spectitle));
        $document->setValue('${specialitye}', $ireport->spectitle);
        $document->setValue('${year}', $ireport->YEAR);
        $document->setValue('${specialitykod}', $ireport->spcode);
        $document->setValue('${developerrole}', $ireport->userRole);
        $document->setValue('${developer}', $ireport->userName . " " . mb_strtoupper(substr($ireport->userSurname,0,2)) . ". " . mb_strtoupper(substr($ireport->userOtch,0,2)) . ". ");
        $document->setValue('${cafedrae}', str_replace("Кафедра", "кафедри", $ireport->deptitle));
        $document->setValue('${credits}', $ireport->CREDITCOUNT);
        $document->setValue('${thours}', $ireport->userRole);
        $document->setValue('${cmodules}', $ireport->MODULESCOUNT);
        $document->setValue('${zmmodules}', $ireport->ZMMODULESCOUNT);
        $document->setValue('${areakod}', $ireport->areaCode);
        $document->setValue('${area}', $ireport->areaName);
        $document->setValue('${purpose}', $ireport->PURPOSE);
        $document->setValue('${task}', $ireport->TASK);
        $document->setValue('${thours}', $ireport->THOURS);
        $document->saveAs(storage_path('result.docx')); //имя заполненного шаблона 
        // $newSection = $wordTest->addSection();
    
        // $desc1 = "The Portfolio details is a very useful feature of the web page. You can establish your archived details and the works to the entire web community. It was outlined to bring in extra clients, get you selected based on this details.";
    
        // $newSection->addText($desc1, array('name' => 'Tahoma', 'size' => 15, 'color' => 'red'));
    
        // $objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($document, 'Word2007');
        // try {
        //     $objectWriter->save(storage_path('helloWorld.docx'));
        // } catch (Exception $e) {
        // }
    
        return response()->download(storage_path('result.docx'))->deleteFileAfterSend(true);
    } 
}
