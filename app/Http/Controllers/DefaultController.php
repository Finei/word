<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use DB;

class DefaultController extends Controller
{
    public function index() {  
        if(auth()->user()->type=='A'){
            return redirect('/user');
        }
        else if(auth()->user()->type=='Z'){
            return redirect('/sreports');
        }
        else if(auth()->user()->type=='P'){
            return redirect('/reports');
        }   
        else if(auth()->user()->type=='G'){
            return redirect('/sreports');
        }  
        else if(auth()->user()->type=='V'){
            return redirect('/sreports');
        }     
        return  redirect('login');
    }
    
}
