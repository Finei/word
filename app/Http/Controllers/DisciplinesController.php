<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use DB;

class DisciplinesController extends Controller
{
    public function index() {        
        $disciplines = DB::select(@"select ds.id, ds.title as dstitle,
        ds.idspec, sp.title as sptitle, sp.spcode,
        dp.id as dpid, dp.title as dptitle
        from disciplines ds
        left join specialities sp
        on sp.id = ds.idspec
        left join departments dp
        on dp.id = sp.iddepart");

        $specialities = DB::select(@"select sp.id, sp.title as sptitle, sp.iddepart,
        dp.title as dptitle
        from specialities sp
        left join departments dp 
        on dp.id = sp.iddepart");

        return view('disciplines', compact('disciplines','specialities'));
    }
    public function create(Request $request) {
        $id = DB::table('disciplines')->insertGetId([
            'title' => $request->dstitle,
            'idspec' => $request->idspec,
        ]);
        return $id;
    }
    public function update(Request $request) {        
        DB::table('disciplines')
            ->where('id', $request->id)        
            ->update([
            'title' => $request->dstitle,
            'idspec' => $request->idspec,
        ]);
        return "success";
    }
    public function delete(Request $request) {
        DB::table('disciplines')->where('id', '=', $request->id)->delete();
        return $request;
    }
}
