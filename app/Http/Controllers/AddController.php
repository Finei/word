<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use DB;

class AddController extends Controller
{
    public function index() {
        $disciplines = DB::select("select * from disciplines");
        $departments = DB::select("select * from departments");
        $specialities = DB::select("select sp.id, sp.title, sp.IDDEPART, sp.SPCODE, sp.AREA_ID, area.ID as ARID, area.NAME, area.CODE  from specialities sp left join area on area.id = sp.area_id");
        $preparations = DB::select("select * from preparations");
        // dump($disciplines);
        return view('addreport', compact('disciplines','departments','specialities','preparations'));
    }
    public function create(Request $request) {
        $reportid = DB::table('report')->insertGetId(
            [
                'developer' => $request->developer,
                'purpose' => $request->goal,
                'task' => $request->task,
                'creditcount' => $request->credits,
                'modulescount' =>  $request->modulesc,
                'zmmodulescount' => $request->zmmodulesc,
                'isnessesory' =>  $request->currnessesiries==='Обов’язкова'?1:0,
                'year' => $request->currentYear,
                'USERID' => $request->userId,
                'INDIVIDUALWORK' => $request->individualWork,
                'iddesc' => $request->currentDiscipline,
                'idspec' => $request->currentSpeciality,
                'idprep' => $request->currentPreparetion,
                'THOURS' => $request->totalHours
            ]
        );

        $agreeament = DB::table('agreement')->insert([
            'idrep' => $reportid,
            'ZKstatus' => 0,
            'GMRstatus' => 0,
            'GNMVstatus' => 0,
        ]);
        
        foreach ($request->practicalWorks as $current) {
            $praktikainsert = DB::table('praktika')->insert([
                'NUMBER' => $current['number'],
                'NAME' => $current['theme'],
                'HORS' => $current['hours'],
                'idrep' => $reportid
            ]);
        }
        
        foreach ($request->years as $current) {
            $pryearinsert = DB::table('PREPYEAR')->insertGetId([
                'NUMBER' => $current['number'],
                'idrep' => $reportid
            ]);
            foreach($current['semestrs'] as $semestr){
                $semestrinsert = DB::table('semestr')->insertGetId([
                    'NUMBER' => $semestr['number'],
                    'TYPEWORK' => $semestr['typeWork'],
                    'TYPEEXAM' => $semestr['typeExam'],
                    'idprepyear' => $pryearinsert
                ]);

                $semestrhoutsinsert = DB::table('hours')->insert([
                    'LHOUR' => $semestr['lektions'],
                    'PRHOUR' => $semestr['praktika'],
                    'LABHOUR' => $semestr['laboratory'],
                    'INDHOUR' => $semestr['Individual'],
                    'SRHOUR' => $semestr['sam'],
                    'IDSEMESTR' => $semestrinsert
                ]);
            }
        }

        foreach ($request->know as $current) {
            $knowinsert = DB::insert("insert 
                into know(NAME,IDREP) 
                values(?,?)",[
                $current,
                $reportid
            ]);           
        }
           
        $balinsert = DB::table('bal')->insert([
            'TEXTBAL' => (int)$request->textbal,
            'GRAPHBAL' => (int)$request->graphbal,
            'SECUREBAL' => (int)$request->securebal,
            'IDREP' => $reportid
        ]);


        foreach($request->beable as $current){
            $beableinsert = DB::table('beable')->insert([
                'NAME' => $current,
                'idrep' => $reportid
            ]);
        }

        foreach ($request->seminarsWorks as $current) {
            $seminarinsert = DB::table('sem')->insert([
                'NUMBER' => $current['number'],
                'NAME' => $current['theme'],
                'HOURS' => $current['hours'],
                'idrep' => $reportid
            ]);
        }
            
        foreach ($request->questions as $quest) {
            $questionins = DB::table('question')->insert([
                'NUMBER' => $quest['number'],
                'THEME' => $quest['theme'],
                'HOURS' => $quest['hours'],
                'IDREP' => $reportid
            ]);
        } 


        foreach ($request->modules as $module) {
            $moduleinsert = DB::table('module')->insertGetId([
                'NUMBER' => $module['number'],
                'NAME' => $module['title'],
                'TASKBAL' => $module['indbal'],
                'EXAMBAL' => $module['exambal'],
                'IDREP' => $reportid
            ]);

            foreach($module['themes'] as $theme){
                $themeinsert = DB::table('theme')->insertGetId([
                    'TITLE' => $theme['title'],
                    'DESCR' => $theme['description'],
                    'BAL' => $theme['bal'],
                    'NUMBER' => $theme['number'],
                    'IDMODULE' => $moduleinsert
                ]);

                $hoursinsert = DB::table('hours')->insert([
                    'LHOUR' => $theme['lhours'],
                    'PRHOUR' => $theme['phours'],
                    'LABHOUR' => $theme['labhours'],
                    'INDHOUR' => $theme['indhours'],
                    'SRHOUR' => $theme['shours'],
                    'IDTHEME' => $themeinsert
                ]);

                foreach($theme['labs'] as $lab){
                    $labinsert = DB::table('lab')->insert([
                        'NAME' => $lab['theme'],
                        'HOURS' => $lab['hours'],
                        'NUMBER' => $lab['number'],
                        'IDTHEME' => $themeinsert,
                        'IDREP' => $reportid
                    ]);                    
                }
            }
        }

        foreach ($request->recommendedBooksBasic as $current) {
            $seminarinsert = DB::table('lit')->insert([
                'NAME' => $current,
                'TYPE' => 0,
                'idrep' => $reportid
            ]);
        }

        foreach ($request->recommendedBooksAdditional as $current) {
            $seminarinsert = DB::table('lit')->insert([
                'NAME' => $current,
                'TYPE' => 1,
                'idrep' => $reportid
            ]);
        }

        foreach ($request->informationResources as $current) {
            $seminarinsert = DB::table('lit')->insert([
                'NAME' => $current,
                'TYPE' => 2,
                'idrep' => $reportid
            ]);
        }
        return "added";
    }

    public function update(Request $request) {
        $reportid = DB::table('report')->where('id', $request->id)->update  (
            [
                'developer' => $request->developer,
                'purpose' => $request->goal,
                'task' => $request->task,
                'creditcount' => $request->credits,
                'modulescount' =>  $request->modulesc,
                'zmmodulescount' => $request->zmmodulesc,
                'isnessesory' =>  $request->currnessesiries==='Обов’язкова'?1:0,
                'year' => $request->currentYear,
                'USERID' => $request->userId,
                'INDIVIDUALWORK' => $request->individualWork,
                'iddesc' => $request->currentDiscipline,
                'idspec' => $request->currentSpeciality,
                'idprep' => $request->currentPreparetion,
                'THOURS' => $request->totalHours
            ]
        );

        $agreeament = DB::table('agreement')->where('idrep', $request->id)->update([
            'ZKstatus' => 0,
            'GMRstatus' => 0,
            'GNMVstatus' => 0
        ]);
        

        $prdelete = DB::table('praktika')->where('idrep', '=', $request->id)->delete();
        foreach ($request->practicalWorks as $current) {
            $praktikainsert = DB::table('praktika')->insert([
                'NUMBER' => $current['number'],
                'NAME' => $current['theme'],
                'HORS' => $current['hours'],
                'idrep' => $request->id
            ]);
        }

        
        $moduledelete =  DB::table('PREPYEAR')->where('IDREP', '=', $request->id)->delete();
        foreach ($request->years as $current) {
            $pryearinsert = DB::table('PREPYEAR')->insertGetId([
                'NUMBER' => $current['number'],
                'idrep' => $request->id
            ]);
            foreach($current['semestrs'] as $semestr){
                $semestrinsert = DB::table('semestr')->insertGetId([
                    'NUMBER' => $semestr['number'],
                    'TYPEWORK' => $semestr['typeWork'],
                    'TYPEEXAM' => $semestr['typeExam'],
                    'idprepyear' => $pryearinsert
                ]);

                $semestrhoutsinsert = DB::table('hours')->insert([
                    'LHOUR' => $semestr['lektions'],
                    'PRHOUR' => $semestr['praktika'],
                    'LABHOUR' => $semestr['laboratory'],
                    'INDHOUR' => $semestr['Individual'],
                    'SRHOUR' => $semestr['sam'],
                    'IDSEMESTR' => $semestrinsert
                ]);
            }
        }
        
        $knowdelete =  DB::table('know')->where('IDREP', '=', $request->id)->delete();
        foreach ($request->know as $current) {
            $knowinsert = DB::insert("insert 
                into know(NAME,IDREP) 
                values(?,?)",[
                $current,
                $request->id
            ]);           
        }
           
        $balinsert = DB::table('bal')->where('IDREP', $request->id)->update([
            'TEXTBAL' => (int)$request->textbal,
            'GRAPHBAL' => (int)$request->graphbal,
            'SECUREBAL' => (int)$request->securebal
        ]);


        $beabledelete =  DB::table('beable')->where('IDREP', '=', $request->id)->delete();
        foreach($request->beable as $current){
            $beableinsert = DB::table('beable')->insert([
                'NAME' => $current,
                'idrep' => $request->id
            ]);
        }

        $semdelete =  DB::table('sem')->where('IDREP', '=', $request->id)->delete();
        foreach ($request->seminarsWorks as $current) {
            $seminarinsert = DB::table('sem')->insert([
                'NUMBER' => $current['number'],
                'NAME' => $current['theme'],
                'HOURS' => $current['hours'],
                'idrep' => $request->id
            ]);
        }
            
        $quedelete =  DB::table('question')->where('IDREP', '=', $request->id)->delete();
        foreach ($request->questions as $quest) {
            $questionins = DB::table('question')->insert([
                'NUMBER' => $quest['number'],
                'THEME' => $quest['theme'],
                'HOURS' => $quest['hours'],
                'IDREP' => $request->id
            ]);
        } 


        $moduledelete =  DB::table('module')->where('IDREP', '=', $request->id)->delete();
        foreach ($request->modules as $module) {
            $moduleinsert = DB::table('module')->insertGetId([
                'NUMBER' => $module['number'],
                'NAME' => $module['title'],
                'TASKBAL' => $module['indbal'],
                'EXAMBAL' => $module['exambal'],
                'IDREP' => $request->id
            ]);

            foreach($module['themes'] as $theme){
                $themeinsert = DB::table('theme')->insertGetId([
                    'TITLE' => $theme['title'],
                    'DESCR' => $theme['description'],
                    'BAL' => $theme['bal'],
                    'NUMBER' => $theme['number'],
                    'IDMODULE' => $moduleinsert
                ]);

                $hoursinsert = DB::table('hours')->insert([
                    'LHOUR' => $theme['lhours'],
                    'PRHOUR' => $theme['phours'],
                    'LABHOUR' => $theme['labhours'],
                    'INDHOUR' => $theme['indhours'],
                    'SRHOUR' => $theme['shours'],
                    'IDTHEME' => $themeinsert
                ]);

                foreach($theme['labs'] as $lab){
                    $labinsert = DB::table('lab')->insert([
                        'NAME' => $lab['theme'],
                        'HOURS' => $lab['hours'],
                        'NUMBER' => $lab['number'],
                        'IDTHEME' => $themeinsert,
                        'IDREP' => $request->id
                    ]);                    
                }
            }
        }

        $quedelete =  DB::table('lit')->where('IDREP', '=', $request->id)->delete();
        foreach ($request->recommendedBooksBasic as $current) {
            $seminarinsert = DB::table('lit')->insert([
                'NAME' => $current,
                'TYPE' => 0,
                'idrep' => $request->id
            ]);
        }

        foreach ($request->recommendedBooksAdditional as $current) {
            $seminarinsert = DB::table('lit')->insert([
                'NAME' => $current,
                'TYPE' => 1,
                'idrep' => $request->id
            ]);
        }

        foreach ($request->informationResources as $current) {
            $seminarinsert = DB::table('lit')->insert([
                'NAME' => $current,
                'TYPE' => 2,
                'idrep' => $request->id
            ]);
        }
        return "added";
    }

    public function select($id) {
        $reports = DB::select(
            @"select r.*, dis.title as distitle, dis.id as disid,
            sp.title as spectitle, sp.spcode, sp.id as specid,
            dep.title as deptitle, dep.id as depid,
            pr.title as prtitle, pr.id as prid,
            users.name as userName, users.surname as userSurname, users.Role as userRole, users.otch as userOtch,
            area.name as areaName, area.CODE as areaCode
               from report r
            left join users on users.id = r.USERID
            left join disciplines dis on dis.id = r.iddesc
            left join specialities sp on sp.id = r.idspec
            left join departments dep on dep.id = sp.iddepart
            left join preparations pr on pr.id = r.idprep
            left join area on area.id = sp.area_id
            where r.id = ?
        ",[$id]);
        $iknow = DB::select(
            @"select * from know where idrep = ?
        ",[$id]);
        $ipraktika = DB::select(
            @"select * from praktika where idrep = ?
        ",[$id]);
        $ibeable = DB::select(
            @"select * from beable where idrep = ?
        ",[$id]);
        $bal = DB::select(
            @"select * from bal where idrep = ?
        ",[$id]);
        $isem = DB::select(
            @"select * from sem where idrep = ?
        ",[$id]);
        $iquestion = DB::select(
            @"select * from question where idrep = ?
        ",[$id]);
        $ilit = DB::select(
            @"select * from lit where idrep = ?
        ",[$id]);
        $iprepyear = DB::select(
            @"select * from prepyear where idrep = ?
        ",[$id]);
        $isemestr = DB::select(
            @"select semestr.*,hours.* from prepyear 
            left join semestr on semestr.idprepyear = prepyear.id
            left join hours on semestr.id = hours.idsemestr
            where idrep = ?
        ",[$id]);
        $imodule = DB::select(
            @"select * from module where idrep = ?
        ",[$id]);
        $itheme = DB::select(
            @"select theme.TITLE,theme.DESCR,theme.BAL,theme.NUMBER,theme.IDMODULE as MIDMODULE,hours.* from module 
            left join theme  on theme.idmodule = module.id 
            left join hours on hours.idtheme = theme.id
            where idrep =?
        ",[$id]);
        $ilab = DB::select(
            @"select lab.* from module 
            left join theme  on theme.idmodule = module.id 
            left join hours on hours.idtheme = theme.id
            left join lab on lab.idtheme = theme.id
            where module.idrep = ?
        ",[$id]);
        $disciplines = DB::select("select * from disciplines");
        $departments = DB::select("select * from departments");
        $specialities = DB::select("select * from specialities");
        $preparations = DB::select("select * from preparations");
        $ireport = array_shift($reports);
        $ibal = array_shift($bal);
        return view('showreport',
        compact('ireport','iknow','ipraktika','ibeable','ibal','isem',
        'iquestion','ilit','iprepyear','isemestr','imodule','itheme','ilab','disciplines','departments','specialities','preparations'));
    }

    public function copy($id) {
        $reports = DB::select(
            @"select r.*, dis.title as distitle, dis.id as disid,
            sp.title as spectitle, sp.spcode, sp.id as specid,
            dep.title as deptitle, dep.id as depid,
            pr.title as prtitle, pr.id as prid,
            users.name as userName, users.surname as userSurname, users.Role as userRole, users.otch as userOtch,
            area.name as areaName, area.CODE as areaCode
               from report r
            left join users on users.id = r.USERID
            left join disciplines dis on dis.id = r.iddesc
            left join specialities sp on sp.id = r.idspec
            left join departments dep on dep.id = sp.iddepart
            left join preparations pr on pr.id = r.idprep
            left join area on area.id = sp.area_id
            where r.id = ?
        ",[$id]);
        $iknow = DB::select(
            @"select * from know where idrep = ?
        ",[$id]);
        $ipraktika = DB::select(
            @"select * from praktika where idrep = ?
        ",[$id]);
        $ibeable = DB::select(
            @"select * from beable where idrep = ?
        ",[$id]);
        $bal = DB::select(
            @"select * from bal where idrep = ?
        ",[$id]);
        $isem = DB::select(
            @"select * from sem where idrep = ?
        ",[$id]);
        $iquestion = DB::select(
            @"select * from question where idrep = ?
        ",[$id]);
        $ilit = DB::select(
            @"select * from lit where idrep = ?
        ",[$id]);
        $iprepyear = DB::select(
            @"select * from prepyear where idrep = ?
        ",[$id]);
        $isemestr = DB::select(
            @"select semestr.*,hours.* from prepyear 
            left join semestr on semestr.idprepyear = prepyear.id
            left join hours on semestr.id = hours.idsemestr
            where idrep = ?
        ",[$id]);
        $imodule = DB::select(
            @"select * from module where idrep = ?
        ",[$id]);
        $itheme = DB::select(
            @"select theme.TITLE,theme.DESCR,theme.BAL,theme.NUMBER,theme.IDMODULE as MIDMODULE,hours.* from module 
            left join theme  on theme.idmodule = module.id 
            left join hours on hours.idtheme = theme.id
            where idrep =?
        ",[$id]);
        $ilab = DB::select(
            @"select lab.* from module 
            left join theme  on theme.idmodule = module.id 
            left join hours on hours.idtheme = theme.id
            left join lab on lab.idtheme = theme.id
            where module.idrep = ?
        ",[$id]);
        $disciplines = DB::select("select * from disciplines");
        $departments = DB::select("select * from departments");
        $specialities = DB::select("select * from specialities");
        $preparations = DB::select("select * from preparations");
        $ireport = array_shift($reports);
        $ibal = array_shift($bal);
        $iscopy = true;
        return view('copyreport',
        compact('ireport','iknow','ipraktika','ibeable','ibal','isem',
        'iquestion','ilit','iprepyear','isemestr','imodule','itheme','ilab','disciplines','departments','specialities','preparations','iscopy'));
    }

    public function show($id) {
        $reports = DB::select(
            @"select r.*, dis.title as distitle, dis.id as disid,
            sp.title as spectitle, sp.spcode, sp.id as specid,
            dep.title as deptitle, dep.id as depid,
            pr.title as prtitle, pr.id as prid,
            users.name as userName, users.surname as userSurname, users.Role as userRole, users.otch as userOtch,
            area.name as areaName, area.CODE as areaCode,
            ag.ZKstatus, ag.GMRstatus, ag.GNMVstatus
               from report r
            left join users on users.id = r.USERID
            left join disciplines dis on dis.id = r.iddesc
            left join specialities sp on sp.id = r.idspec
            left join departments dep on dep.id = sp.iddepart
            left join preparations pr on pr.id = r.idprep
            left join area on area.id = sp.area_id
            left join agreement ag on r.id = ag.idrep
            where r.id = ?
        ",[$id]);
        $iknow = DB::select(
            @"select * from know where idrep = ?
        ",[$id]);
        $ipraktika = DB::select(
            @"select * from praktika where idrep = ?
        ",[$id]);
        $ibeable = DB::select(
            @"select * from beable where idrep = ?
        ",[$id]);
        $bal = DB::select(
            @"select * from bal where idrep = ?
        ",[$id]);
        $isem = DB::select(
            @"select * from sem where idrep = ?
        ",[$id]);
        $iquestion = DB::select(
            @"select * from question where idrep = ?
        ",[$id]);
        $ilit = DB::select(
            @"select * from lit where idrep = ?
        ",[$id]);
        $iprepyear = DB::select(
            @"select * from prepyear where idrep = ?
        ",[$id]);
        $isemestr = DB::select(
            @"select semestr.*,hours.* from prepyear 
            left join semestr on semestr.idprepyear = prepyear.id
            left join hours on semestr.id = hours.idsemestr
            where idrep = ?
        ",[$id]);
        $imodule = DB::select(
            @"select * from module where idrep = ?
        ",[$id]);
        $itheme = DB::select(
            @"select theme.TITLE,theme.DESCR,theme.BAL,theme.NUMBER,theme.IDMODULE as MIDMODULE,hours.* from module 
            left join theme  on theme.idmodule = module.id 
            left join hours on hours.idtheme = theme.id
            where idrep =?
        ",[$id]);
        $ilab = DB::select(
            @"select lab.* from module 
            left join theme  on theme.idmodule = module.id 
            left join hours on hours.idtheme = theme.id
            left join lab on lab.idtheme = theme.id
            where module.idrep = ?
        ",[$id]);
        $disciplines = DB::select("select * from disciplines");
        $departments = DB::select("select * from departments");
        $specialities = DB::select("select * from specialities");
        $preparations = DB::select("select * from preparations");
        $ireport = array_shift($reports);
        $ibal = array_shift($bal);
        return view('showelreport',
        compact('ireport','iknow','ipraktika','ibeable','ibal','isem',
        'iquestion','ilit','iprepyear','isemestr','imodule','itheme','ilab','disciplines','departments','specialities','preparations'));
    }
}
