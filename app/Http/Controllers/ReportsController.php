<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use DB;

class ReportsController extends Controller
{
   public function index() {        
      $reports = DB::select(
         @"     select disciplines.title as discipline, specialities.title as speciality, specialities.spcode as specialityCode,
         departments.title as department,report.id, report.year as year, area.name as area,
        agreement.id as idagree, agreement.ZKstatus as ZKstatusAgree,  agreement.GMRstatus as GMRstatusAgree,  agreement.GNMVstatus as GNMVstatusAgree,
         acZK.comment as ZKcomment,acGMR.comment as GMRcomment,acGNMV.comment as GNMVcomment
         from report 
         left join disciplines
            on disciplines.id = report.iddesc
         left join specialities
            on specialities.id = report.idspec
         left join departments 
            on departments.id = specialities.iddepart
         left join area 
            on area.id = specialities.area_id
         left join agreement
            on agreement.idrep = report.id
         left join agreecomment acZK
            on agreement.id = acZK.idagree and acZK.typeagree = 1
         left join agreecomment acGMR
            on agreement.id = acGMR.idagree and acGMR.typeagree = 2
         left join agreecomment acGNMV
            on agreement.id = acGNMV.idagree and acGNMV.typeagree = 3 
         where report.userid = ?",[\Auth::user()->id]);
      return view('reports', compact('reports'));
   }
   public function showzav(Request $request) {
      $reports = DB::select(
         @"select disciplines.title as discipline, specialities.title as speciality, specialities.spcode as specialityCode,
         departments.title as department,report.id, report.year as year, area.name as area,
         agreement.id idagree, agreement.ZKstatus as ZKstatusAgree,
         agreement.GMRstatus as GMRstatusAgree, agreement.GNMVstatus as GNMVstatusAgree
            from report 
            left join disciplines
               on disciplines.id = report.iddesc
            left join specialities
               on specialities.id = report.idspec
            left join departments 
               on departments.id = specialities.iddepart
            left join area 
               on area.id = specialities.area_id
            left join agreement
               on agreement.idrep = report.id ");
      return view('sreports', compact('reports'));
   }
   public function agree(Request $request) {
      if(\Auth::user()->type=='Z'){
         $agreeament = DB::table('agreement')->where('idrep',$request->id)->update([
            'ZKstatus' => 1
         ]);
      }
      else if(\Auth::user()->type=='G'){
         $agreeament = DB::table('agreement')->where('idrep',$request->id)->update([
            'GMRstatus' => 1
         ]);
      }
      else if(\Auth::user()->type=='V'){
         $agreeament = DB::table('agreement')->where('idrep',$request->id)->update([
            'GNMVstatus' => 1
         ]);
      }
      return 1;
   }
   public function notagree(Request $request) {      
      if(\Auth::user()->type=='Z'){
         $agreeament = DB::table('agreement')->where('idrep',$request->id)->update([
            'ZKstatus' => 2
         ]);
         $agreecomment = DB::table('agreecomment')->updateOrInsert([
            'idagree' => $request->idagree,
            'typeagree' => $request->typeagree
         ],[
            'comment' => $request->comment
         ]);
      }
      else if(\Auth::user()->type=='G'){
         $agreeament = DB::table('agreement')->where('idrep',$request->id)->update([
            'GMRstatus' => 2
         ]);
         $agreecomment = DB::table('agreecomment')->updateOrInsert([
            'idagree' => $request->idagree,
            'typeagree' => $request->typeagree
         ],[
            'comment' => $request->comment
         ]);
      }
      else if(\Auth::user()->type=='V'){
         $agreeament = DB::table('agreement')->where('idrep',$request->id)->update([
            'GNMVstatus' => 2
         ]);
         $agreecomment = DB::table('agreecomment')->updateOrInsert([
            'idagree' => $request->idagree,
            'typeagree' => $request->typeagree
         ],[
            'comment' => $request->comment
         ]);
      }
      return 2;
   }
}
