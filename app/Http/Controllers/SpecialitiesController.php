<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use DB;

class SpecialitiesController extends Controller
{
    public function index() {        
        $specialities = DB::select(@"select sp.id, sp.title as sptitle,sp.spcode,
        sp.iddepart, dp.title as dptitle, a.id as areaid, a.name
        from specialities sp
        left join departments dp on sp.iddepart = dp.id
        left join area a on sp.area_id = a.id ");

        $departments = DB::select("select * from departments");
        $area = DB::select("select * from area");

        return view('specialities', compact('specialities','departments', 'area'));
    }
    public function create(Request $request) {
        $id = DB::table('specialities')->insertGetId([
            'title' => $request->sptitle,
            'iddepart' => $request->iddepart,
            'spcode' => $request->spcode,
            'area_id' => $request->areaid,
        ]);
        return $id;
    }
    public function update(Request $request) {        
        DB::table('specialities')
            ->where('id', $request->id)        
            ->update([
            'title' => $request->sptitle,
            'iddepart' => $request->iddepart,
            'spcode' => $request->spcode,
            'area_id' => $request->areaid,
        ]);
        return "success";
    }
    public function delete(Request $request) {
        DB::table('specialities')->where('id', '=', $request->id)->delete();
        return $request;
    }
}
