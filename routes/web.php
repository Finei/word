<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function(){    
    Route::group(['middleware' => 'admin'], function(){ 
        Route::get('/user', 'UsersController@index')->name('user');
        Route::get('/departments', 'DepartmentsController@index')->name('departments');
        Route::get('/areas', 'AreasController@index')->name('areas');
        Route::get('/specialities', 'SpecialitiesController@index')->name('specialities');
        Route::get('/disciplines', 'DisciplinesController@index')->name('disciplines');
        Route::post('/addUser', 'UsersController@create');
        Route::post('/updateUser', 'UsersController@update');
        Route::post('/deleteUser', 'UsersController@delete');
        Route::post('/addKafedra', 'DepartmentsController@create');
        Route::post('/updateKafedra', 'DepartmentsController@update');
        Route::post('/deleteKafedra', 'DepartmentsController@delete');
        Route::post('/addSpeciality', 'SpecialitiesController@create');
        Route::post('/updateSpeciality', 'SpecialitiesController@update');
        Route::post('/deleteSpeciality', 'SpecialitiesController@delete');
        Route::post('/addDiscipline', 'DisciplinesController@create');
        Route::post('/updateDiscipline', 'DisciplinesController@update');
        Route::post('/deleteDiscipline', 'DisciplinesController@delete');
        Route::post('/addAreas', 'AreasController@create');
        Route::post('/updateAreas', 'AreasController@update');
        Route::post('/deleteAreas', 'AreasController@delete');
    });
    Route::get('404', function () {
        return abort(404);
    });
    Route::post('/enter', 'AuthController@enter');
    
    Route::get('/addReport', 'AddController@index');
    Route::get('/addReport/{id}', 'AddController@select');
    Route::get('/copyReport/{id}', 'AddController@copy');
    Route::get('/showReport/{id}', 'AddController@show');
    Route::get('/saveReport/{id}', 'WordController@createWordDocx');
    
    Route::post('/addReport', 'AddController@create');
    Route::post('/updateReport', 'AddController@update');
    Route::post('/agreeReport', 'ReportsController@agree');
    Route::post('/notagreeReport', 'ReportsController@notagree');
    
    Route::get('/reports', 'ReportsController@index')->name('reports');
    Route::get('/sreports', 'ReportsController@showzav')->name('sreports');
    Route::post('/addData', 'AddController@create');
    Route::get('/', 'DefaultController@index');
});



// Route::get('/', function () {
//     return view('welcome');
// });



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
