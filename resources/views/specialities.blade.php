@extends('layouts.app')

@section('content')
    <specialities
        :specialities="{{json_encode($specialities)}}"
        :departments = "{{json_encode($departments)}}"
        :area = "{{json_encode($area)}}">
    </specialities>
@endsection
