<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .navbar-laravel{
            background: #85b6f9;
        }
        #navbarSupportedContent .mr-auto li{
            font-size:16px;
        }
        body{
            word-break: normal !important; 
        }
        nav{
    
            padding: 0 !important;
        }
        .nav-item{
            font-family: 'Century Gothic', Arial, sans-serif;
            padding: 17px;
    font-weight: 800;
            transition: .3s ease-out 0.2s;
        }
        .active{
            background: linear-gradient(90deg, rgba(104,146,204,0.5) 0%, rgba(104,146,204,0.95) 50%, rgba(104,146,204,0.5) 100%);
        
        }
        .nav-item:hover{
            background: linear-gradient(90deg, rgba(104,146,204,0.5) 0%, rgba(104,146,204,0.95) 50%, rgba(104,146,204,0.5) 100%);
        }

    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <!-- <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button> -->

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @guest

                        @else
                            @if (Auth::user()->type=='A')
                                <li class="nav-item {{ request()->route()->getName() === 'user' ? 'active': 'non-active'}}">
                                    <a class="nav-link" href="{{ route('user') }}">{{ __('Користувачі') }}</a>
                                </li> 
                                <li class="nav-item {{ request()->route()->getName() === 'departments' ? 'active': 'non-active'}}">
                                    <a class="nav-link" href="{{ route('departments') }}">{{ __('Кафедри') }}</a>
                                </li>
                                <li class="nav-item {{ request()->route()->getName() === 'specialities' ? 'active': 'non-active'}}">
                                    <a class="nav-link" href="{{ route('specialities') }}">{{ __('Спеціальності') }}</a>
                                </li>
                                <li class="nav-item {{ request()->route()->getName() === 'disciplines' ? 'active': 'non-active'}}">
                                    <a class="nav-link" href="{{ route('disciplines') }}">{{ __('Дисципліни') }}</a>
                                </li>
                                <li class="nav-item {{ request()->route()->getName() === 'areas' ? 'active': 'non-active'}}">
                                    <a class="nav-link" href="{{ route('areas') }}">{{ __('Галузі') }}</a>
                                </li>
                            @endif
                            @if (Auth::user()->type=='P')                            
                                <li class="nav-item {{ request()->route()->getName() === 'reports' ? 'active': 'non-active'}}">
                                    <a class="nav-link" href="{{ route('reports') }}">{{ __('Робочі програми') }}</a>
                                </li>
                            @endif
                            @if (Auth::user()->type=='Z')
                                <li class="nav-item {{ request()->route()->getName() === 'sreports' ? 'active': 'non-active'}}">
                                    <a class="nav-link" href="{{ route('sreports') }}">{{ __('Робочі програми') }}</a>
                                </li>
                            @endif
                            @if (Auth::user()->type=='G')
                                <li class="nav-item {{ request()->route()->getName() === 'sreports' ? 'active': 'non-active'}}">
                                    <a class="nav-link" href="{{ route('sreports') }}">{{ __('Робочі програми') }}</a>
                                </li>
                            @endif
                            @if (Auth::user()->type=='V')
                                <li class="nav-item {{ request()->route()->getName() === 'sreports' ? 'active': 'non-active'}}">
                                    <a class="nav-link" href="{{ route('sreports') }}">{{ __('Робочі програми') }}</a>
                                </li>
                            @endif
                        @endguest
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Увійти') }}</a>
                            </li>
                            <!-- @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif -->
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} {{ Auth::user()->surname }}<span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Вийти') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main >
            @yield('content')
        </main>
    </div>
</body>
</html>
