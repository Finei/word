
@extends('layouts.app')

@section('content')
<div id="app">
    <add-report
        :disciplines="{{json_encode($disciplines)}}"
        :departments="{{json_encode($departments)}}"
        :specialities="{{json_encode($specialities)}}"
        :preparations="{{json_encode($preparations)}}"
        :currentuser='{!! Auth::user()->toJson() !!}'>
    </add-report>
</div>
@endsection
