
@extends('layouts.app')

@section('content')
<div id="app">
    <show-report
    :ireport="{{json_encode($ireport)}}"
    :iknow="{{json_encode($iknow)}}"
    :ipraktika="{{json_encode($ipraktika)}}"
    :ibeable="{{json_encode($ibeable)}}"
    :ibal="{{json_encode($ibal)}}"
    :isem="{{json_encode($isem)}}"
    :iquestion="{{json_encode($iquestion)}}"
    :ilit="{{json_encode($ilit)}}"
    :iprepyear="{{json_encode($iprepyear)}}"
    :isemestr="{{json_encode($isemestr)}}"
    :imodule="{{json_encode($imodule)}}"
    :itheme="{{json_encode($itheme)}}"
    :ilab="{{json_encode($ilab)}}"
    :disciplines="{{json_encode($disciplines)}}"
    :departments="{{json_encode($departments)}}"
    :specialities="{{json_encode($specialities)}}"
    :preparations="{{json_encode($preparations)}}"    
    >
    </show-report>
</div>
@endsection
