@extends('layouts.app')

@section('content')
    <users
        :users="{{json_encode($users)}}"
        :departments="{{json_encode($departments)}}">
    </users>
@endsection
