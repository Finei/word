@extends('layouts.app')

@section('content')
    <sreports
        :reports="{{json_encode($reports)}}"
        :currentuser='{!! Auth::user()->toJson() !!}'>
    </sreports>
@endsection
