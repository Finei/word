@extends('layouts.app')

@section('content')
    <disciplines
        :disciplines = "{{json_encode($disciplines)}}"
        :specialities = "{{json_encode($specialities)}}">
    </disciplines>
@endsection
