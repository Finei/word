@extends('layouts.app')

@section('content')
    <areas
        :areas="{{json_encode($areas)}}">
    </areas>
@endsection
