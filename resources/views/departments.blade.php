@extends('layouts.app')

@section('content')
    <departments
        :departments="{{json_encode($departments)}}">
    </departments>
@endsection
