@extends('layouts.app')

@section('content')
    <reports
        :reports="{{json_encode($reports)}}">
    </reports>
@endsection
