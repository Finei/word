
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue';
import iView from 'iview';
import 'iview/dist/styles/iview.css';
import Vuelidate from 'vuelidate';

Vue.use(Vuelidate);
Vue.use(iView);


require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('add-report', require('./components/AddReport.vue').default);
Vue.component('show-report', require('./components/ShowReport.vue').default);
Vue.component('auth', require('./components/Auth.vue').default);
Vue.component('users', require('./components/Users.vue').default);
Vue.component('departments', require('./components/Departments.vue').default);
Vue.component('specialities', require('./components/Specialities.vue').default);
Vue.component('disciplines', require('./components/Disciplines.vue').default);
Vue.component('areas', require('./components/Areas.vue').default);
Vue.component('reports', require('./components/Reports.vue').default);
Vue.component('sreports', require('./components/SReports.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
